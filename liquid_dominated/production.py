# -*- coding: utf-8 -*-
# Documentation : https://charms.gitlabpages.inria.fr/ComPASS/

import numpy as np
import ComPASS
from ComPASS.utils.units import *
from ComPASS.timeloops import standard_loop, TimeStepManager
import ComPASS.mpi as mpi
from ComPASS.newton import Newton
from ComPASS.utils.salome import SalomeWrapper


# -------------------------------------------------------------------
# Set output informations
ComPASS.set_output_directory_and_logfile(__file__)

# -------------------------------------------------------------------
# Parameters
k_matrix : float = 1e-18              # matrix permeability in m^2
phi_matrix : float = 0.05             # matrix porosity
k_fracture : float = 5e-14            # fracture permeability in m^2
k_aquifer : float = 1e-14             # reservoir permeability in m^2
phi_fracture : float = 0.2            # fracture porosity
lambda_th : float = 2                 # bulk thermal conductivity kg.m.s^-3.K^-1 or W/m/K

heat_capacity_rock : float = 1000.0   # rock specific heat capacity J.kg^-1.K^-1  
rho_rock_density : float = 2600.0     # Rock density kg.m^-3
gravity : float = 9.80665             # m/s^2
fracture_thickness: float = 10.0      # fracture thickness in m

well_radius : float = 0.115           # well radius in m
Qm: float = 180 * ton / hour          # production/injection flowrate in kg/s
Tinjection: float = degC2K(110)       # injection temperature - convert Celsius to Kelvin degrees


with_bottom_heat_flux: bool = False
kill_top_fracture_permeability: bool = True
use_well: bool = False


# -------------------------------------------------------------------
# Load the water2ph physics : it contains the water component
# which can be in liquid and/or gas phase
simulation = ComPASS.load_eos("water2ph")

# -------------------------------------------------------------------
# Set gravity
simulation.set_gravity(gravity)


# -------------------------------------------------------------------
# Import the mesh created with Salome
sw = SalomeWrapper(simulation)


# -------------------------------------------------------------------
# Fracture factory
# set the fracture thickness (global variable)
simulation.set_fracture_thickness(fracture_thickness)


# -------------------------------------------------------------------
# Create the doublet : Producer x1 & Injector x1 
# using groups of nodes (groups: well2 & well3)
injector_id3 : int = 2
producer_id : int = 3
wellids = producer_id, injector_id3

# We must always create wells to have the same mesh parts
def create_wells():
    def _well_from_nodes(nodes, well_id):
        z = simulation.global_vertices()[nodes, 2]
        well = simulation.create_single_branch_well(
            nodes[np.argsort(z)[::-1]], well_radius
        )
        well.id = well_id
        return well

    producer = _well_from_nodes(sw.info.well3.nodes, producer_id)
    producer.operate_on_flowrate = 1, 1 * bar
    producer.produce()
    
    injector3 = _well_from_nodes(sw.info.well2.nodes, injector_id3)
    injector3.operate_on_flowrate = -1, 100*MPa
    injector3.inject(degC2K(Tinjection))

    return [producer, injector3]


# -------------------------------------------------------------------
# Set subdomains permeability using the cells groups
def set_permeabilities():
    k = np.full(sw.info.mesh.nb_cells, k_matrix)
    k[sw.info.reservoir.cells] = k_aquifer
    return k



# -------------------------------------------------------------------
# Set fracture permeability using the faces groups
def set_fracture_permeability():
    zc = simulation.compute_global_face_centers()[:, 2]
    zc = zc[sw.info.fault.faces]
    k = np.full(zc.shape[0], k_fracture)
    if kill_top_fracture_permeability:
        k[zc >= zbot_cap] = 1e-5 * k_fracture 
    return k


# -------------------------------------------------------------------
# Identify the Dirichlet nodes (at the top and the bottom)
def set_dirichlet():
    where = np.zeros(simulation.global_vertices().shape[0], dtype=np.bool)
    where[sw.info.top.nodes] = True
    if not with_bottom_heat_flux:
        where[sw.info.bottom.nodes] = True
    return where


# -------------------------------------------------------------------
# Initialize the regionalized values and distribute the domain
simulation.init(
    mesh = sw.mesh,
    wells = create_wells,
    cell_permeability = set_permeabilities,
    cell_porosity = phi_matrix,
    cell_thermal_conductivity = lambda_th,
    fracture_thermal_conductivity = lambda_th,
    fracture_faces = lambda: sw.info.fault.faces,
    fracture_permeability = set_fracture_permeability,
    fracture_porosity = phi_fracture,
    set_dirichlet_nodes = set_dirichlet,
    set_global_flags = sw.flags_setter,
    well_model = "two_phases",
)

# linked to the data distribution and flags
sw.rebuild_locally()


# -------------------------------------------------------------------
# reload the last state of snapshot_directory (initial state)
simulation.reload_snapshot("output-L_HER_init")


# -------------------------------------------------------------------
# Dirichlet nodes will be locked to their equilibrium values
all_nodes = [
    nodes
    for nodes in [
        sw.info.top.nodes,
        sw.info.bottom.nodes,
    ]
    if nodes is not None
]
boundaries = np.unique(np.hstack(all_nodes))
simulation.reset_dirichlet_nodes(boundaries)
# simulation.reset_dirichlet_nodes(sw.info.bottom.nodes)
# simulation.reset_dirichlet_nodes(sw.info.top.nodes)


# -------------------------------------------------------------------
# Close wells
for wid in wellids:
    simulation.close_well(wid)

# -------------------------------------------------------------------
# Open the producer & Set its operating condition 
simulation.open_well(producer_id)
simulation.set_well_property(producer_id, imposed_flowrate = Qm)


# -------------------------------------------------------------------
# Define time steps parameters
tsmger1 = TimeStepManager(
    initial_timestep = 1.,
    increase_factor = 1.5,
    decrease_factor = 0.5,
)

# -------------------------------------------------------------------
# Execute the first time loop : 3 years
final_time1 = 3*year #
simulation.standard_loop(
    initial_time = 0,
    final_time = final_time1,
    time_step_manager = tsmger1,
    specific_outputs = [1., 1*day, 0.083333*year], 
    output_period = 0.2*year,
)


# -------------------------------------------------------------------
# Open the injector & Set its operating condition    
simulation.open_well(injector_id3)
simulation.set_well_property(
    injector_id3, imposed_flowrate = -0.8*Qm, injection_temperature=Tinjection)


# -------------------------------------------------------------------
# Execute the second time loop : 17 years
final_time2 = final_time1 + 17*year
simulation.standard_loop(
    initial_time = final_time1,
    final_time = final_time2,
    time_step_manager = tsmger1,
    specific_outputs = [1.+final_time2, 1*day+final_time2, 0.083333*year+final_time2, 0.25*year+final_time2],
    output_period = 0.5*year,
)


# -------------------------------------------------------------------
# postprocessing, it allows to visualize with Paraview
simulation.postprocess() 