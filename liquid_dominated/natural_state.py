# -*- coding: utf-8 -*-
# Documentation : https://charms.gitlabpages.inria.fr/ComPASS/

import numpy as np
import ComPASS
from ComPASS.utils.units import *
from ComPASS.timeloops import standard_loop, TimeStepManager
import ComPASS.mpi as mpi
from ComPASS.newton import Newton
from ComPASS.linalg.petsc_linear_solver import *
from ComPASS.linalg.factory import linear_solver
from ComPASS.utils.salome import SalomeWrapper

# -------------------------------------------------------------------
# Set output informations
ComPASS.set_output_directory_and_logfile(__file__)

# -------------------------------------------------------------------
# Parameters
ptop : float = 1 * bar                # initial reservoir pressure
Tair : float = degC2K(30.0)           # initial reservoir temperature - convert Celsius to Kelvin degrees
k_matrix : float = 1e-18              # matrix permeability in m^2
phi_matrix : float = 0.05             # matrix porosity
k_fracture : float = 5e-14            # fracture permeability in m^2
k_aquifer : float = 1e-14             # reservoir permeability in m^2
phi_fracture : float = 0.2            # fracture porosity
lambda_th : float = 2                 # bulk thermal conductivity kg.m.s^-3.K^-1 or W/m/K

heat_capacity_rock : float = 1000.0   # rock specific heat capacity J.kg^-1.K^-1  
rho_rock_density : float = 2600.0     # Rock density kg.m^-3
gravity : float = 9.80665             # m/s^2
fracture_thickness: float = 10.0      # fracture thickness in m
ztop: float = 1000.0                  # z maximum value in m
zbottom: float = 0.                   # z minimum value in m
ztop_reservoir: float = ztop - 50.0   # top of the reservoir (z coordinate) in m
zbot_cap: float = ztop_reservoir      # bottom of the caprock (z coordinate) in m
Tbot_cap: float = degC2K(200.0)       # initial temperature at the bottom of the caprock - convert Celsius to Kelvin degrees
Tbottom: float = 550.                 # initial temperature at the bottom of the domain

well_radius : float = 0.115           # well radius in m
Qm: float = 180 * ton / hour          # production/injection flowrate in kg/s
Tinjection: float = degC2K(110)       # injection temperature - convert Celsius to Kelvin degrees


with_bottom_heat_flux: bool = False
kill_top_fracture_permeability: bool = True
use_well: bool = False


# -------------------------------------------------------------------
# Load the water2ph physics : it contains the water component
# which can be in liquid and/or gas phase
simulation = ComPASS.load_eos("water2ph")

# -------------------------------------------------------------------
# Set gravity
simulation.set_gravity(gravity)


# -------------------------------------------------------------------
# Import the mesh created with Salome
sw = SalomeWrapper(simulation)


# -------------------------------------------------------------------
# Fracture factory
# set the fracture thickness (global variable)
simulation.set_fracture_thickness(fracture_thickness)


# -------------------------------------------------------------------
# Create the doublet : Producer x1 & Injector x1 
# using groups of nodes (groups: well2 & well3)
injector_id3 : int = 2
producer_id : int = 3
wellids = producer_id, injector_id3

# We must always create wells to have the same mesh parts
def create_wells():
    def _well_from_nodes(nodes, well_id):
        z = simulation.global_vertices()[nodes, 2]
        well = simulation.create_single_branch_well(
            nodes[np.argsort(z)[::-1]], well_radius
        )
        well.id = well_id
        return well

    producer = _well_from_nodes(sw.info.well3.nodes, producer_id)
    producer.operate_on_flowrate = 1, 1 * bar
    producer.produce()
    
    injector3 = _well_from_nodes(sw.info.well2.nodes, injector_id3)
    injector3.operate_on_flowrate = -1, 100*MPa
    injector3.inject(degC2K(Tinjection))

    return [producer, injector3]


# -------------------------------------------------------------------
# Set subdomains permeability using the cells groups
def set_permeabilities():
    k = np.full(sw.info.mesh.nb_cells, k_matrix)
    k[sw.info.reservoir.cells] = k_aquifer
    return k


# -------------------------------------------------------------------
# Set fracture permeability using the faces groups
def set_fracture_permeability():
    zc = simulation.compute_global_face_centers()[:, 2]
    zc = zc[sw.info.fault.faces]
    k = np.full(zc.shape[0], k_fracture)
    if kill_top_fracture_permeability:
        k[zc >= zbot_cap] = 1e-5 * k_fracture 
    return k


# -------------------------------------------------------------------
# Identify the Dirichlet nodes (at the top and the bottom)
def set_dirichlet():
    where = np.zeros(simulation.global_vertices().shape[0], dtype=np.bool)
    where[sw.info.top.nodes] = True
    if not with_bottom_heat_flux:
        where[sw.info.bottom.nodes] = True
    return where


# -------------------------------------------------------------------
# Initialize the regionalized values and distribute the domain
simulation.init(
    mesh = sw.mesh,
    wells = create_wells,
    cell_permeability = set_permeabilities,
    cell_porosity = phi_matrix,
    cell_thermal_conductivity = lambda_th,
    fracture_thermal_conductivity = lambda_th,
    fracture_faces = lambda: sw.info.fault.faces,
    fracture_permeability = set_fracture_permeability,
    fracture_porosity = phi_fracture,
    set_dirichlet_nodes = set_dirichlet,
    set_global_flags = sw.flags_setter,
    well_model = "two_phases",
)

# linked to the data distribution and flags
sw.rebuild_locally()


# Close the wells
if use_well:
    if close_perforations_above_reservoir:
        for wid in wellids:
            simulation.close_perforations(wid, above=ztop_reservoir)
        wellinfo = list(simulation.producers_information())
        for ik, info in enumerate(wellinfo):
            for v, WI in zip(info.vertices, info.well_index_Darcy):
                print(
                    "Proc",
                    mpi.proc_rank,
                    "well",
                    ik,
                    "vertex",
                    v,
                    "WI:",
                    simulation.vertices()[v],
                    WI,
                )
else:
    for wid in wellids:
        simulation.close_well(wid)


# -------------------------------------------------------------------
# Setting up initial values
Ttop = Tair
X0 = simulation.build_state(simulation.Context.liquid, p=ptop, T=Ttop)
rho = simulation.liquid_molar_density(ptop, Ttop)
p = lambda z: ptop + rho * gravity * (ztop - z)

if Tbottom is None:
    def T(z):
        return (z - zbot_cap) * (Tair - Tbot_cap) / (ztop - zbot_cap) + Tbot_cap
else:
    def T(z):
        return z * (Tair - Tbottom) / ztop + Tbottom

if mpi.is_on_master_proc:
    print("Air temperature:", K2degC(Tair), "°C")
    print("Top temperature:", K2degC(T(ztop)), "°C")
    print("Bottom caprock temperature:", K2degC(T(zbot_cap)), "°C")
    print("Bottom temperature:", K2degC(T(zbottom)), "°C")
    print("Bottom pressure:", p(zbottom), "Pa")

if with_bottom_heat_flux:
    bottom_heat_flux = (
        bulk_thermal_conductivity * (Tbot_cap - Tair) / (ztop - zbot_cap)
    )
    if mpi.is_on_master_proc:
        print("Bottom heat flux:", bottom_heat_flux)
    Neumann = ComPASS.NeumannBC()
    Neumann.heat_flux = bottom_heat_flux
    if sw.info.bottom.faces is not None:
        simulation.set_Neumann_faces(sw.info.bottom.faces, Neumann)

def set_states(states, z):
    states.set(X0)
    states.p[:] = p(z)
    states.T[:] = T(z)

set_states(simulation.node_states(), simulation.vertices()[:, 2])
set_states(simulation.cell_states(), simulation.cell_centers()[:, 2])
set_states(
    simulation.fracture_states(), simulation.compute_fracture_centers()[:, 2]
)
simulation.reset_dirichlet_nodes_states()
assert np.all(simulation.dirichlet_node_states().p == simulation.node_states().p)
assert np.all(simulation.dirichlet_node_states().T == simulation.node_states().T)


# -------------------------------------------------------------------
# Define solver parameters
lsolver = linear_solver(simulation, direct=False)
newton = Newton(simulation, 1e-5, 8, lsolver)


# -------------------------------------------------------------------
# Define time steps parameters
tsmger = TimeStepManager(
    initial_timestep= 3600.,
    increase_factor=2,
    decrease_factor=1 / 1.5,
    karma_threshold=10,
    karma_reset=False,
)


# -------------------------------------------------------------------
# Execute the time loop
final_time = 1e5*year
simulation.standard_loop(
    final_time = final_time,
    time_step_manager = tsmger,
    newton = newton,
    output_after_loop = True,
    output_before_start = True,
)


# -------------------------------------------------------------------
# postprocessing, it allows to visualize with Paraview
simulation.postprocess()

