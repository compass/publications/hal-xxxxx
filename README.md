# Scripts used to produce paper results

## Useful links

[paper submitted version](paper_link_to_be_added)

[ComPASS documentation](https://charms.gitlabpages.inria.fr/ComPASS)

## Structure of the directories

There are two directories:
- *liquid_dominated* corresponds to section 3.1 of the [paper](paper_link_to_be_added)
- *steam_dominated* corresponds to section 3.2 of the [paper](paper_link_to_be_added)

Two scripts must be run sequentially:
- `natural_state.py` will compute the so-called natural state of the geothermal system
- `production.py` will compute the state of the reservoir corresponding to the explotation scenario

Mesh files were produced using the Salome platform and are to be read with ComPASS.utils.salome.SalomeWrapper.

They comprise:
- `GROUPS.py`: groups of elements (nodes, faces and tetras) corresponding to geological domains, well paths...
- `NODES.txt`: mesh nodes (ID and coordinates)
- `TETRAS.txt`: tetrahedra (nodes IDs)