# -*- coding: utf-8 -*-
# Documentation : https://charms.gitlabpages.inria.fr/ComPASS/

import numpy as np
import ComPASS
import ComPASS.mpi as mpi
from ComPASS.utils.units import *
from ComPASS.utils.various import tensor_coordinates
import ComPASS.io.mesh as io
from ComPASS.timeloops import standard_loop, TimeStepManager


# -------------------------------------------------------------------
# Load mesh and simulation parameters from an external file
import loader_init as loader
simulation = loader.setup(__file__)


# -------------------------------------------------------------------
# Create the doublet : Producer x1 & Injector x1 
# using groups of nodes from GROUPS.py
for wid in [simulation.params.injector_id, simulation.params.producer_id]:
    simulation.close_well(wid)


# -------------------------------------------------------------------
# Export initial states and properties to visualize with Paraview
# you are free to uncomment (or add) lines to export additional parameters
node_states = simulation.node_states()
cell_states = simulation.cell_states()
rho = simulation.liquid_molar_density
petrophysics = simulation.petrophysics()

pointdata={
    "dirichlet pressure": simulation.pressure_dirichlet_values(),
    "dirichlet temperature": K2degC(simulation.temperature_dirichlet_values()),
    # "initial pressure": node_states.p,
    # "initial temperature": K2degC(node_states.T),
    # "liquid density": rho(node_states.p, node_states.T),
}
celldata={
    "initial pressure": cell_states.p,
    "initial temperature": K2degC(cell_states.T),
    "phi": petrophysics.cell_porosity,
    # "k" : petrophysics.cell_permeability,
    # "clg" : petrophysics.cell_thermal_conductivity
}
celldata.update(
    tensor_coordinates(petrophysics.cell_permeability, "k", diagonal_only=True)
)
celldata.update(
    tensor_coordinates(petrophysics.cell_thermal_conductivity, "C", diagonal_only=True)
)
io.write_mesh(simulation, "initial_states", pointdata=pointdata, celldata=celldata)


# -------------------------------------------------------------------
# Execute the initial time loop : 10000 years
final_time = 1e4 * year
output_period = 0.5 * final_time

simulation.standard_loop(
   final_time = final_time,
   output_period=output_period,
   time_step_manager=TimeStepManager(
       initial_timestep= 5., 
       increase_factor=2.0, 
       decrease_factor=0.5, 
       karma_threshold=10,
       karma_reset=False,
   ),
)


# -------------------------------------------------------------------
# postprocessing, it allows to visualize with Paraview
simulation.postprocess()
