# -*- coding: utf-8 -*-
# Documentation : https://charms.gitlabpages.inria.fr/ComPASS/

import numpy as np
import ComPASS
import ComPASS.mpi as mpi
from ComPASS.utils.units import *
from ComPASS.utils.various import tensor_coordinates
import ComPASS.io.mesh as io
from ComPASS.timeloops import standard_loop, TimeStepManager
from collections import defaultdict
import loader_prod as loader


# -------------------------------------------------------------------
# Load mesh, simulation parameters and previous results from an external file
simulation = loader.setup(__file__)


# -------------------------------------------------------------------
# Defintition of a connection between source (producer, id = 1) & target well (injector, id =0)
doublets = np.array([[simulation.params.producer_id, simulation.params.injector_id]])
all_wells = set(doublets.ravel())
nb_well_pairs = 1
proc_requests = [(mpi.master_proc_rank, all_wells)]
simulation.add_well_connections(well_pairs=doublets, proc_requests=proc_requests)

def chain_wells(tick):
    for source, target in doublets:
        inj_data = simulation.get_well_data(target)
        if inj_data is not None:
            wellhead = simulation.well_connections[source]
            print('well_inj_flowrate_before:', inj_data.imposed_flowrate)
            print('well_prod_flowrate:', wellhead.molar_flowrate)
            assert (
                wellhead.molar_flowrate >=0
            ), f"source well {source} should be a producer"
            inj_data.imposed_flowrate = -wellhead.molar_flowrate
            print('well_inj_flowrate_after:', inj_data.imposed_flowrate)
            #get the delta T of the nearest current time to define the injection temperature
            inj_data.injection_temperature = simulation.params.Tinj    
            # print('Prod_temp: ', wellhead.temperature)
            # print('Inj_temp: ', data.injection_temperature)

iteration_callbacks = [chain_wells]

if mpi.is_on_master_proc:
    # This will be kept in memory for the whole simulation
    # you may need to flush data to a file at some point
    wellhead_states = defaultdict(list)
    def collect_wellhead_states(tick):
        for well in all_wells:
            wellhead_states[well].append(
                [tick.time, *simulation.well_connections[well]]
            )
    iteration_callbacks.append(collect_wellhead_states)  # only on master proc...                                   

# -------------------------------------------------------------------
# Execute the second time loop : Exploitation over 30 years
final_time = 30 * year
output_period = 1 * year

simulation.standard_loop(
   final_time = final_time,
   output_period = output_period,
   time_step_manager = TimeStepManager(
       initial_timestep = 1., 
       increase_factor = 2.0, 
       decrease_factor = 0.5, 
       karma_threshold = 10,
       karma_reset = False,
   ),
   specific_outputs = [1.],
   iteration_callbacks = iteration_callbacks, 
)


# -------------------------------------------------------------------
# postprocessing, it allows to visualize with Paraview
simulation.postprocess()
