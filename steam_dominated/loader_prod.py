# -*- coding: utf-8 -*-
# Documentation : https://charms.gitlabpages.inria.fr/ComPASS/

import sys
import os
from os import path
import pathlib
import numpy as np

import dataclasses

import ComPASS
import ComPASS.mpi as mpi
from ComPASS.utils.units import *
from ComPASS.utils.various import tensor_coordinates
import ComPASS.io.mesh as io
from ComPASS.utils.salome import SalomeWrapper
from collections import defaultdict


# -------------------------------------------------------------------
# Parameters
@dataclasses.dataclass
class Params:
    ptop: float = 1.0 * bar            # initial reservoir pressure
    Ttop: float = degC2K(20.0)         # initial top reservoir temperature
    Tres: float = degC2K(295)          # initial reservoir temperature
    Sgres: float = 0.9                 # initial reservoir gas saturation
    kres: float = 1e-14                # reservoir permeability in m^2
    kcap: float = 1e-20                # caprock permeability in m^2
    phires: float = 0.035              # reservoir porosity
    phicap : float = 0.015             # caprock porosity
    K: float = 2.0                     # bulk thermal conductivity kg.m.s^-3.K^-1 or W/m/K
    H: float = 4000.0                  # domain height in m
    caprock_thickness: float = 0.5 * H # caprock thickness in m
    Lx: float = 2000                   # domain width in m
    Ly: float = 2000                   # domain lenght in m
    gravity: float = 9.81              # m/s^2
    Qm: float = 75 * ton / hour        # production/injection flowrate in kg/s
    well_radius: float = 0.115         # well radius in m
    Tinj: int = degC2K(110)            # injection temperature - convert Celsius to Kelvin degrees
    injector_id = 0                    # id injector
    producer_id = 1                    # id producer
    

def setup(output_path=None, **kwargs):
    # -------------------------------------------------------------------
    # Load the water2ph physics : it contains the water component
    # which can be in liquid and/or gas phase    
    simulation = ComPASS.load_eos("water2ph")

    
    # -------------------------------------------------------------------
    # Import the mesh created with Salome
    sw = SalomeWrapper(simulation)


    # -------------------------------------------------------------------
    # Import the parameters    
    params = Params(**kwargs)
    simulation.params = params
    gravity = params.gravity
    Qm = params.Qm
    

    # -------------------------------------------------------------------
    # Set gravity    
    simulation.set_gravity(gravity)


    # -------------------------------------------------------------------
    # Set output informations
    ComPASS.set_output_directory_and_logfile(output_path)


    # -------------------------------------------------------------------
    # Create the doublet : Producer x1 & Injector x1     
    wellids = params.injector_id, params.producer_id

    # We must always create wells to have the same mesh parts
    def create_wells():
        def _well_from_nodes(nodes, well_id):
            # print(f"Creating well {well_id} with nodes: {nodes}")
            z = simulation.global_vertices()[nodes, 2]
            well = simulation.create_single_branch_well(
                nodes[np.argsort(z)[::-1]], params.well_radius
            )
            well.id = well_id
            return well
    
        injector = _well_from_nodes(sw.info.well1.nodes, params.injector_id)
        injector.operate_on_flowrate = -params.Qm, 1e8*bar
        injector.inject(params.Tinj)
        producer = _well_from_nodes(sw.info.well0.nodes, params.producer_id)
        producer.operate_on_flowrate = params.Qm, 1 * bar
        producer.produce()
        return [injector, producer]
    
    # -------------------------------------------------------------------
    # Set subdomains permeability using the cells groups
    def set_permeabilities():
        k = np.full(sw.info.mesh.nb_cells, params.kres)
        k[sw.info.caprock.cells] = params.kcap
        return k

    # -------------------------------------------------------------------
    # Set subdomains porosity using the cells groups    
    def porosity():
        poro_cells = np.full(sw.info.mesh.nb_cells, params.phires)
        poro_cells[sw.info.caprock.cells] = params.phicap
        return poro_cells

    # -------------------------------------------------------------------
    # Identify the Dirichlet nodes    
    def set_dirichlet_nodes():
        where = np.zeros(sw.mesh.nb_vertices, dtype=np.bool)
        where[sw.info.topNodes.nodes] = True
        return where 


    # -------------------------------------------------------------------
    # Initialize the regionalized values and distribute the domain
    simulation.init(
        mesh = sw.mesh,
        wells = create_wells,
        cell_permeability = set_permeabilities,
        cell_porosity = porosity,
        cell_thermal_conductivity = params.K,
        set_dirichlet_nodes = set_dirichlet_nodes,
        set_global_flags = sw.flags_setter,
        well_model = "two_phases",
        )
    
    
    # linked to the data distribution and flags
    sw.rebuild_locally()
    
    
    # -------------------------------------------------------------------
    # Setting up initial values
    
    # Neumann at the bottom faces
    bottom_heat_flux = params.K*(params.Tres-params.Ttop)/params.caprock_thickness
    print(f"Bottom heat flux: {bottom_heat_flux} W/m^2")
    Neumann = ComPASS.NeumannBC()
    Neumann.heat_flux = bottom_heat_flux
    face_centers = simulation.face_centers()
    simulation.set_Neumann_faces(face_centers[:, 2] <= -params.H, Neumann)

    # Reload previous states : reload_snapshot("output-xxxx", iteration = X)'''
    simulation.reload_snapshot("output-S_HER_init")

    # Dirichlet nodes will be locked to their equilibrium values '''
    simulation.reset_dirichlet_nodes(sw.info.topNodes.nodes)

    return simulation


